# build
docker build -t scola-converter .

# run local
docker run -p 5000:5000 -it --rm --name app-scola-converter scola-converter

# push to docker hub
docker images

docker tag 1c0be35afd18 randiekas/scola-converter:latest

docker push randiekas/scola-converter:latest

# clean images
docker image prune
