from flask import Flask, request, redirect
from werkzeug.utils import secure_filename
from flask_restful import Resource, Api
import datetime
from random import randint
#import
import requests
from tests.docx import to_latex
import zipfile
from lxml import etree
import tempfile
import os
import shutil
import logging
from logging.handlers import TimedRotatingFileHandler
import json
import boto3

#initilize aws
client = boto3.client(
    's3',
    aws_access_key_id="AKIAJQ3FP3UL6CHL57DA",
    aws_secret_access_key="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",
)

# initialize logging capability
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

log_handler = TimedRotatingFileHandler('log/backend-status.log', when="midnight", interval=1)
log_handler.setLevel(logging.INFO)

log_formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
log_handler.setFormatter(log_formatter)

log_handler.suffix = "%Y%m%d"

logger.addHandler(log_handler)
def dump(obj):
   for attr in dir(obj):
       if hasattr( obj, attr ):
           print( "obj.%s = %s" % (attr, getattr(obj, attr)))

try: 
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup

def html_to_json(content, indent=None):
    soup = BeautifulSoup(content, "lxml")
    rows = soup.find_all("tr")
    
    headers = {}
    thead = soup.find("thead")
    if thead:
        thead = thead.find_all("th")
        for i in range(len(thead)):
            headers[i] = thead[i].text.strip().lower()
    data = []
    index = 0 
    for row in rows:
        cells = row.find_all("td")
        if thead:
            try:
                items = {}
                items['level'] = str(cells[0].text)
                items['bobot'] = str(cells[1].text)
                items['tipe_soal'] = str(cells[2].text)
                items['pertanyaan'] = str(cells[3]).replace("<td><p>","<p>").replace("</p></td>","</p>")
                items['jawaban'] = str(cells[4].text.strip())
                items['a'] = str(cells[5]).replace("<td><p>","<p>").replace("</p></td>","</p>")
                items['b'] = str(cells[6]).replace("<td><p>","<p>").replace("</p></td>","</p>")
                items['c'] = str(cells[7]).replace("<td><p>","<p>").replace("</p></td>","</p>")
                items['d'] = str(cells[8]).replace("<td><p>","<p>").replace("</p></td>","</p>")
                items['e'] = str(cells[9]).replace("<td><p>","<p>").replace("</p></td>","</p>")
            except:
                # return "Tidak boleh ada tabel dalam tabel, cek soal nomor "+str(index-1)+", Review kembali syarat dan ketentuan yang telah ada di file import disini : https://assets.scola.id/questions/0_format_import_questions_new.docx"
                # return redirect("https://assets.scola.id/converter/warning.html")
                return False
                pass
        else:
            items = []
            for index in cells:
                items.append(index.text.strip())
        data.append(items)
        index += 1
    return data


app = Flask(__name__)
api = Api(app)

response = {}

class Converter(Resource):
    def get(self):
        return {'status':'success'}

    def post(self):
        #https://dev.scola.id//api/classroom_quiz/import/82?filename=82_equation_simple.docx
        #request.form['tes']="sdf"
        
        f               = request.files['file']

        if f.mimetype == "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
            file_name       = str(datetime.datetime.now().time())+"_"+str(randint(0, 100))+"_"+secure_filename(f.filename)
            
            # client.upload_fileobj(f, "scola-converter", "raws/2020-06-23/"+file_name)

            f.save("uploads/"+file_name)
            to_latex(filename="uploads/"+file_name,result='static/results/'+file_name.replace(".docx",''))
            #$link_api 			= str_replace(" ","%20","{$api}/{$id_question_bank}?filename=".$filename);
            #file_get_contents($link_api);
            id_quiz         = request.form['id_quiz']
            lessons         = request.form['lessons']
            api             = request.form['api']+"/"+id_quiz+"?filename="+file_name.replace('.docx','.html')
            if "api-" not in api:
                api         = api.replace("https:","http:")
            

            params = dict(filename=file_name.replace('.docx','.html'))
            # batsa
            content         = open("static/results/"+file_name.replace('.docx','.html'),"r")
            content_full    = content.read()
            
            content_full    = content_full.replace("<table>","<table><thead>"
                                "<th>level</th>"
                                "<th>bobot</th>"
                                "<th>tipe_soal</th>"
                                "<th>pertanyaan</th>"
                                "<th>jawaban</th>"
                                "<th>a</th>"
                                "<th>b</th>"
                                "<th>c</th>"
                                "<th>d</th>"
                                "<th>e</th>"
                            "</thead>")
            content_full    = content_full.replace("<p><p>","<p>")
            content_full    = content_full.replace("</p></p>","</p>")
            json    = html_to_json(content_full)

            os.remove("uploads/"+file_name)
            os.remove("static/results/"+file_name)
            os.remove("static/results/"+file_name.replace(".docx",".html"))

            if json:
                # batas
                resp = requests.get(url=api)
                #data = resp.json() # Check the JSON Response Content documentation below
                logger.info(api+"#"+str(resp.status_code)+"#"+resp.text)
                return redirect(request.form['redirect'])
            else:
                return redirect("https://assets.scola.id/converter/warning.html")
        else:
            # return "Mohon maaf anda belum mengikuti format terbaru dari kami. Silakan download disini (nge link ke halaman donlod format) untuk format terbaru"
            return redirect("https://assets.scola.id/converter/warning.html")
        #return redirect(api)
        #return api

class Parsing(Resource):
    def get(self):
        content         = requests.get("https://scola-converter.s3-ap-southeast-1.amazonaws.com/results/"+datetime.datetime.today().strftime('%Y-%m-%d')+"/"+request.args['filename'].replace(".docx",'.html'))
        content_full    = content.text

        content_full    = content_full.replace("<table>","<table><thead>"
                            "<th>level</th>"
                            "<th>bobot</th>"
                            "<th>tipe_soal</th>"
                            "<th>pertanyaan</th>"
                            "<th>jawaban</th>"
                            "<th>a</th>"
                            "<th>b</th>"
                            "<th>c</th>"
                            "<th>d</th>"
                            "<th>e</th>"
                        "</thead>")
        content_full    = content_full.replace("<p><p>","<p>")
        content_full    = content_full.replace("</p></p>","</p>")
        
        json    = html_to_json(content_full)
        content.close()
        return json

api.add_resource(Converter, '/convert')
api.add_resource(Parsing, '/parsing')
# returns list of objects as header is present
# [{"id": "1", "vendor": "Intel", "product": "Processor"}, {"id": "2", "vendor": "AMD", "product": "GPU"}, {"id": "3", "vendor": "Gigabyte", "product": "Mainboard"}]

#print(html_to_json(content_no_thead, indent=4))
if __name__ == '__main__':
    app.debug=True
    app.run(host='0.0.0.0', port=5000)